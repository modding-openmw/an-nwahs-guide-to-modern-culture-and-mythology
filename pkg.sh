#!/bin/sh
set -eu

file_name=an-nwahs-guide-to-modern-culture-and-mythology.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

zip --must-match --recurse-paths $file_name \
    an-nwahs-guide-to-modern-culture-and-mythology.omwaddon \
    CHANGELOG.md \
    icons \
    LICENSE \
    meshes \
    README.md \
    sound \
    textures \
    version.txt
sha256sum $file_name > $file_name.sha256sum.txt
sha512sum $file_name > $file_name.sha512sum.txt
