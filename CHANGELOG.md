## An N'wah's Guide To Modern Culture And Mythology Changelog

#### 1.9

* Increased johnnyhostile's wander distance by roughly 4x
* His script won't disable him until the player isn't looking at him

[Download Link](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/packages/25170509) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)

#### 1.8

* Fixed a bug that caused johnnyhostile to never go away like he said he would
* Gave the book mesh and textures unique names/paths so they don't clash with upstream

[Download Link](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/packages/23925797) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)

#### 1.7

* Fixed packaging bug.

[Download Link](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/packages/23581839) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)

#### 1.6

* Added a unique book for the guide (thanks to Settyness and Sophie!)

[Download Link](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/packages/23581775) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)

#### 1.5

* Increase dialogue track volume

[Download Link](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/packages/23326402) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)

#### 1.4

* New entries by Hurdrax Custos
* Removed the entry for Rise of the Tribe Unmourned
* Moved the NPC more towards the bridge
* The NPC now wanders
* The NPC no longer force greets
* Fixed dialogue issues
* Cleaned up the scripts and variables used

[Download Link](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/packages/21655596) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)

#### 1.3

* Properly implemented the audio via `Say` (special thanks to Settyness for this!)

[Download Link](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/packages/14927493) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)

#### 1.2

* Cleaned up audio tracks a bit (special thanks to Settyness for this!)

[Download Link](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/packages/14911282) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)

#### 1.1

* Re-recorded the audio tracks at a slightly higher level and with some cleanup.
* The odd NPC now has a secondary greeting after he's already greeted you and gave the book
* Removed the "psst" since it was too easy to overlap with the actual greeting.
* Actually included the audio files
* Fixed a bug where the player could receive multiple copies of the book.

[Download Link](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/packages/14877270) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)

#### 1.0

* Initial release of the mod.

[Download Link](https://gitlab.com/modding-openmw/an-nwahs-guide-to-modern-culture-and-mythology/-/packages/14848545) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52942)
